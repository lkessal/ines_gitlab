package view.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import view.MazeApp;

public final class SaveMenuItem extends JMenuItem implements ActionListener {

		private MazeApp mazeApp;
		private JOptionPane popupMessage;
		
		public SaveMenuItem(MazeApp mazeApp) {
			super("Save the current maze");
			addActionListener(this);
			this.mazeApp = mazeApp;
			popupMessage = new JOptionPane();
			
		}

		@Override
		public void actionPerformed(ActionEvent event){
			try {
				System.out.println("maze saved");
				mazeApp.getModele().saveMaze();
				popupMessage.showMessageDialog(mazeApp, "Maze saved in data/savedMaze.txt");
			}catch(FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
}
