package view.panels;

import java.awt.GridLayout;

import javax.swing.JPanel;

import view.MazeApp;
import view.SelectTypeBoxLabel;
import view.buttons.ChooseBoxLabelButton;

public final class ButtonsPanel extends JPanel {
	
		private DimensionPanel dimensionPanel;
		private ChooseBoxLabelButton eButton;
		private ChooseBoxLabelButton wButton;
		private ChooseBoxLabelButton dButton;
		private ChooseBoxLabelButton aButton;
		private SelectTypeBoxLabel selectTypeBoxLabel;
		
		public ButtonsPanel(MazeApp mazeApp) {
			
			setLayout(new GridLayout(1,7));
			//add(dimensionPanel = new DimensionPanel(mazeApp));
			add(selectTypeBoxLabel = new SelectTypeBoxLabel(mazeApp));
			add(eButton= new ChooseBoxLabelButton(mazeApp, "Empty"));
			add(dButton= new ChooseBoxLabelButton(mazeApp, "Departure"));
			add(wButton= new ChooseBoxLabelButton(mazeApp, "Wall"));
			add(aButton= new ChooseBoxLabelButton(mazeApp, "Arrival"));
			
		}
		
		public void notifyForUpdate() {
			dimensionPanel.notifyForUpdate();
		}
}
