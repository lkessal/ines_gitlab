package dijkstra;

public interface PiInterface {
	
	//returns pi(vertex) = weight of a shortest path from departure to vertex 
	public void setPi(VertexInterface vertex, int shortValuation);
	//getter of pi(vertex)
	public int getPi (VertexInterface vertex);
}
