package view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import maze.ABox;
import maze.DBox;
import maze.EBox;
import maze.WBox;
import view.panels.BoxPanel;

public final class BoxMouseListener extends MouseAdapter implements MouseListener {

	private MazeApp mazeApp;

	public BoxMouseListener(MazeApp mazeApp) {
		super();
		this.mazeApp = mazeApp;
	}
	
	@Override
	public void mouseReleased(MouseEvent e){
		String boxSelectedType = mazeApp.getModele().getSelectedTypeBox();
		BoxPanel boxPanelClicked = (BoxPanel) e.getSource();
		int abscissa = boxPanelClicked.getAbscissa();
		int ordinate = boxPanelClicked.getOrdinate();
		//no other choice than a switch because boxSelectedType comes from an exterior intermediate (the user)
		switch(boxSelectedType) {
			case "Arrival": boxPanelClicked.setBox(new ABox(abscissa, ordinate, true,"A", mazeApp.getModele().getMaze()));
							mazeApp.getModele().setMazeBox(boxPanelClicked.getBox());
							break;
			case "Departure": boxPanelClicked.setBox(new DBox(abscissa, ordinate, true,"D", mazeApp.getModele().getMaze()));
							mazeApp.getModele().setMazeBox(boxPanelClicked.getBox());
							break;
			case "Wall": boxPanelClicked.setBox(new WBox(abscissa, ordinate, true,"W", mazeApp.getModele().getMaze()));
						mazeApp.getModele().setMazeBox(boxPanelClicked.getBox());
						break;
						
			case "Empty": boxPanelClicked.setBox(new EBox(abscissa, ordinate, true,"E", mazeApp.getModele().getMaze()));
						mazeApp.getModele().setMazeBox(boxPanelClicked.getBox());
						break;
		}
		System.out.println("cc");
	}
}
