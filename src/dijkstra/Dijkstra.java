package dijkstra;

import java.util.ArrayList;

import maze.MBox;
import maze.Maze;
import sun.security.provider.certpath.Vertex;

/*Class dijkstra represents the dijkstra algorithm (declared final to optimize compilation)*/
public final class Dijkstra {

	
	private static PreviousInterface dijkstra (GraphInterface g, VertexInterface r,ASetInterface a,PiInterface pi,PreviousInterface previous) {
		
		a.add(r);
		VertexInterface pivot = r;
		pi.setPi(r,0);
		ArrayList<VertexInterface> allVertices = g.getAllVertices();
		
		/*initializing the vertices' valuations to Integer.MAX_VALUE, symbolizing the positive infinite (no need to implement an infinite function as Integer.MAX_VALUE = 2 147 483 647 is enough)*/
		for (VertexInterface vertex : allVertices) {
			pi.setPi(vertex, Integer.MAX_VALUE);
		}
		pi.setPi(r,0);
		System.out.println ("nb total sommet " + allVertices.size());
		
		/*repeating n times the process (n representing the number of vertices in the graph)*/
		for (int j=0; j<allVertices.size(); j++) {
			
			/* actualizing the valuation pi and father of the pivots' successors*/
			ArrayList<VertexInterface> successorsPivot = g.getSuccessors(pivot);
			System.out.println("iteration "+ j+ " nb successeurs" + successorsPivot.size());
			for (VertexInterface vertex : successorsPivot){
				if (a.contains(vertex) == false ) {
					if (pi.getPi(pivot)+g.getWeight(pivot, vertex) < pi.getPi(vertex)) {
						pi.setPi(vertex, pi.getPi(pivot)+g.getWeight(pivot, vertex));
						previous.setFather(vertex, pivot);
					}
				}
			}
			
			/*determining the next pivot for the next iteration*/
			int piMinimum = Integer.MAX_VALUE; /*usually used to symbolize the infinite instead of implementing a whole function for it*/
			VertexInterface vertex = null;
			for (VertexInterface vertexY : allVertices) {
				if (a.contains(vertexY) == false){
					if (pi.getPi(vertexY)<piMinimum) {
						piMinimum = pi.getPi(vertexY);
						vertex=vertexY;
					}
				}
			}
			/*actualizing the new pivot and adding it to aSet*/
			if (vertex != null) {
				pivot=vertex;
				a.add(pivot); 
			}
					
			/*if no new pivot was found, the algorithm stops and previous is returned to reconstitute the shortest path*/
			else {return previous;}
			System.out.println("pivot iteration " +j);
		}
		/* returning previous to reconstitute the shortest path*/
		Previous previous2 = (Previous) previous;
		System.out.println("nb sommets: "+ previous2.getTable().size());
		return previous;
	
	}
	
	public static PreviousInterface dijkstra(GraphInterface g, VertexInterface r) {
		/*calling the class method dijkstra*/
		return dijkstra (g, r, new ASet(), new Pi(), new Previous());
	}
}