package view.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import maze.MazeReadingException;
import view.MazeApp;

public final class LoadMenuItem extends JMenuItem implements ActionListener{
	
	private MazeApp mazeApp;
	private JOptionPane popupMessage;

	public LoadMenuItem(MazeApp mazeApp) {
		super("Load a new maze to solve");
		this.mazeApp = mazeApp;
		addActionListener(this);
		this.popupMessage = new JOptionPane();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
			try {
				mazeApp.getModele().loadMaze("data/loadNewMaze.txt");
			} catch (IOException e) { // compilator indicated FileNotFoundException is not reachable here so no need to catch it
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			popupMessage.showMessageDialog(mazeApp,"Maze loaded from data/LoadNewMaze.txt");
	}

}
