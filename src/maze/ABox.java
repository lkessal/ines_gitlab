package maze;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import dijkstra.VertexInterface;

/*ABox class represents the properties of the maze's arrival box*/

public final class ABox extends MBox {

	private JPanel boxPanel;
	
	public ABox (int abscissa, int ordinate, boolean accessibility,String label,Maze maze) {
		super(abscissa,ordinate,true,"A", maze);
	}
	
	public ABox (boolean accessibility,String label,Maze maze) {
		super(true,"A", maze);
	}
	
	public void setBackground(JPanel boxPanel, boolean escapeIsLaunched) {
			 boxPanel.setBackground(Color.orange);
			 this.boxPanel = boxPanel;;
	}

	public JPanel getBoxPanel() {
		return boxPanel;
	}
}
