package maze;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;

import dijkstra.GraphInterface;
import dijkstra.Previous;
import dijkstra.VertexInterface;
import modele.Modele;
/* Maze class implements the graph (declared final to optimize compilation)*/

public final class Maze implements GraphInterface{
	
	private MBox [][] maze;
	private  int width; // number of columns
	private  int height; // number of lines
	private  MBox departure;
	private  MBox arrival;
	private ArrayList<VertexInterface> verticesOfShortestPath;
	private Modele modele;
	
	/*constructors to initialize the maze*/
	
	public Maze(int width, int height){
		this.maze = new MBox [height][width];
		this.width = width;
		this.height = height;
	}
	
	public Maze(int width, int height, Modele modele){
		this.maze = new MBox [height][width];
		this.width = width;
		this.height = height;
		this.modele = modele;
	}
	
	
	/*getters to get the maze, a maze's box, the width, length of the maze, and the departure/arrival box*/
	public final MBox[][] getMaze(){
		return maze;
	}
	
	public void setBox (MBox box, int abscissa, int ordinate) {
		this.maze[ordinate][abscissa] = box;
	}
	
	public final MBox getBox(int abscissa, int ordinate) {
		return maze[abscissa][ordinate];
	}
	
	public final int getWidth() {
		return width;
	}
	
	public final int getHeight() {
		return height;
	}
	
	public final MBox getDeparture() {
		return departure;
	}
	
	public final MBox getArrival() {
		return arrival;
	}
	
	public ArrayList<VertexInterface> getVerticesOfShortestPath() {
		return verticesOfShortestPath;
	}
	
	public void setVerticesOfShortestPath(ArrayList<VertexInterface> verticesOfShortestPath) {
		this.verticesOfShortestPath = verticesOfShortestPath;
	}

	/*procedure to get a Vertex's successors*/
	public final ArrayList<VertexInterface> getSuccessors(VertexInterface vertex){
		ArrayList<VertexInterface> successors = new ArrayList<VertexInterface>();
		MBox box = (MBox)vertex;
		/* éventuellement créer une exception si accessibility = false?*/
		if (box.getAccessibility()!=false) {
			int abscissa = box.getAbscissa();
			int ordinate = box.getOrdinate();
			
			try {
			if (maze[ordinate][abscissa+1].getAccessibility()) {
				successors.add(maze[ordinate][abscissa+1]); // adding the right neighbor if it it exits and is accessible
			}}catch(IndexOutOfBoundsException exception) {}; // if the "Index Out Of Bounds" exception is thrown, catch it and carry on the next instructions
			
			try {
			if (maze[ordinate][abscissa-1].getAccessibility()) {
				successors.add(maze[ordinate][abscissa-1]); // adding the  left neighbor if it exits and is accessible
			}}catch(IndexOutOfBoundsException exception) {};
			
			try {
			if (maze[ordinate+1][abscissa].getAccessibility()) {
				successors.add(maze[ordinate+1][abscissa]);  //adding the lower neighbor if it it exits and is accessible
			}}catch(IndexOutOfBoundsException exception) {};
			
			try { 
			if (maze[ordinate-1][abscissa].getAccessibility()) {
				successors.add(maze[ordinate-1][abscissa]); //adding the upper neighbor if it it exits and is accessible
			}}catch(IndexOutOfBoundsException exception) {};
		}
		return successors;
	}
	
	/*getting an edge's weight*/	
	public final int getWeight(VertexInterface src,VertexInterface dst) {
		
		ArrayList<VertexInterface> successors= this.getSuccessors(src);
		if (successors.contains(dst)== true) {
			return 1;
		}
		else return 0;
	}
	
	/*getting all the maze's vertices */
	public final ArrayList<VertexInterface> getAllVertices(){
		
		ArrayList<VertexInterface> allVertices = new ArrayList<VertexInterface>();
		for (int i=0; i<height; i++) {
			for (int j=0; j<width; j++) {
				allVertices.add(maze[i][j]);
			}
		}
		return allVertices;
	}
	
	public final void initFromTextFile(String fileName) throws IOException, FileNotFoundException, Exception  {;
		
		try ( Reader reader = new FileReader (fileName);
			BufferedReader buffer = new BufferedReader (reader);
		){
			String line = new String ();
			int numberOfArrival = 0;
			int numberOfDeparture = 0;
			
			/*while ((line = buffer.readLine())!= null) {
				System.out.println(line);
			}
			buffer.close();*/
			
			/* reading each maze's line*/
			for (int i=0; i<height; i++) {
				line = buffer.readLine();
				
				if (line == null) {
					modele.initMazeError("Le nombre de lignes du fichier texte est inférieur à la hauteur du labyrinthe");
					throw new MazeReadingException(fileName, i+1, "Le nombre de lignes du fichier texte est inférieur à la hauteur du labyrinthe");
				}
				if (line.length() > width) {
					modele.initMazeError("Le nombre de caractère de la ligne " +(i+1)+ " est supérieur à la largeur du labyrinthe");
					throw new MazeReadingException(fileName, i+1, "Le nombre de caractère de la ligne " +(i+1)+ " est supérieur à la largeur du labyrinthe" );
				}
				if (line.length() < width) {
					modele.initMazeError("Le nombre de caractère de la ligne " +(i+1)+ " est inférieur à la largeur du labyrinthe");
					throw new MazeReadingException(fileName, i+1, "Le nombre de caractère de la ligne " +(i+1)+ " est inférieur à la largeur du labyrinthe" );
				}
				else {
					/* reading each character of the line number i*/
					for (int j=0; j<width; j++) {
						char box = line.charAt(j);
						
						/*initializing the boxes of the line number i using the text file's characters*/
						switch(box) {
							case 'A' : if (numberOfArrival==0) {
											maze[i][j] = new ABox(j,i,true,"A", this);
											arrival = maze[i][j];
											numberOfArrival++;
										}
										else {
											modele.initMazeError("Le caractère numéro " +(j+1)+ " de la ligne " +(i+1)+ " est incorrect : il y a déjà une case arrivée dans le labyrinthe");
											throw new MazeReadingException(fileName, i+1, "Le caractère numéro " +(j+1)+ " de la ligne " +(i+1)+ " est incorrect : il y a déjà une case arrivée dans le labyrinthe");
										}	
										break;
							case 'D' : if (numberOfDeparture == 0) {
											maze[i][j] = new DBox(j,i,true,"D", this);
			 							    departure = maze[i][j];
			 							    numberOfDeparture++;
										}
										else {
											modele.initMazeError ("Le caractère numéro " +(j+1)+ " de la ligne " +(i+1)+ " est incorrect : il y a déjà une case départ dans le labyrinthe");
											throw new MazeReadingException(fileName, i+1, "Le caractère numéro " +(j+1)+ " de la ligne " +(i+1)+ " est incorrect : il y a déjà une case départ dans le labyrinthe");
										}	
										break;
							case 'E' : maze[i][j] = new EBox(j,i,true,"E", this);
										System.out.println ("Laby ref =" + maze[i][j]);
										break;
							case 'W' : maze[i][j] = new WBox(j,i,false,"W", this); break;
							default : modele.initMazeError("Le caractère numéro " +(j+1)+ " de la ligne " +(i+1)+ " est incorrect. Veuillez le remplacer par un A,D,E ou W");
									 throw new MazeReadingException(fileName, i+1, "Le caractère numéro " +(j+1)+ " de la ligne " +(i+1)+ " est incorrect. Veuillez le remplacer par un A,D,E ou W");
						}
					}
					//lineCounter = lineCounter+1;
				}
			}
			
			if (numberOfDeparture !=1) {
				modele.initMazeError("Veuillez ajouter une case départ dans le labyrinthe");
				throw new MazeReadingException(fileName,10, "Veuillez ajouter une case départ dans le labyrinthe");
			}
			if (numberOfArrival !=1) {
				modele.initMazeError("Veuillez ajouter une case arrivée dans le labyrinthe");
				throw new MazeReadingException(fileName,10, "Veuillez ajouter une case arrivée dans le labyrinthe");
			}
			
			buffer.close();
			reader.close();
			//if (lineCounter < height) { throw new MazeReadingException(fileName, lineCounter, "Le nombre de lignes du fichier texte est inférieur à la hauteur du labyrinthe");}
			//if (lineCounter > height) { throw new MazeReadingException(fileName, height, "Le nombre de lignes du fichier texte est supérieur à la hauteur du labyrinthe");}
			/*try{  
				buffer.close();
			}catch(Exception e) {};
			try {
				reader.close();
			}catch(Exception e) {};*/
			
			for (int i=0; i<height; i++) {
				for (int j=0; j<width; j++) {
					System.out.print(maze[i][j].getLabel());
				}
				System.out.print ("\n");
			}
			//System.out.println (lineCounter);
		}catch (Exception e) { e.printStackTrace();}
	}
	
	public final void saveToTextFile(String fileName) throws FileNotFoundException {
	try (
		PrintWriter writer = new PrintWriter(fileName);
		)
	{
		/* writing each box's label into fileName*/
		for (int i=0; i<height; i++) {
			for (int j=0; j<width; j++) {
				if (maze[i][j]!=null) {
					writer.print (maze[i][j].getLabel());
				}
				else {
					modele.initMazeError("Veuillez initialiser toutes les cases avant de vous échapper du labyrinthe");
					return;
				}
			}
			writer.print("\n");
		}
		writer.close();
		} catch ( FileNotFoundException exception) { System.out.println ("Fichier inexistant");};
	}
	
	public final void saveShortestPath() throws FileNotFoundException {
		try (PrintWriter printWriter = new PrintWriter(new FileOutputStream ("data/graphePlusCourtChemin.txt")); 
		){
		
			for (int i =0; i<this.getHeight(); i++) {
				for (int j =0; j<this.getWidth(); j++) {
					if (maze[i][j].getLabel()=="A" ||maze[i][j].getLabel()=="D") {
						printWriter.write(maze[i][j].getLabel());
					}
					else {
						if (verticesOfShortestPath.contains(maze[i][j])){
						printWriter.write(".");
						}
						else {
							printWriter.write(maze[i][j].getLabel());
						}
					}
				}
				printWriter.println();
			}
			printWriter.close();
		} catch(Exception e){ 
			e.printStackTrace();
		}
	}


	public void setWidth(int widthOfMaze) {
		this.width = widthOfMaze;
		
	}


	public void setHeight(int heightOfMaze) {
		this.height = heightOfMaze;
		
	}
	
	public void setMaze (MBox[][] maze) {
		this.maze = maze;
	}
	
}
