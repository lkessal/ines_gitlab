package maze;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import dijkstra.VertexInterface;

/*DBox class represents the properties of the maze's departure box*/ 

public final class DBox extends MBox {
	
	private JPanel boxPanel;

	/*calling the super constructor to initialize DBox objects*/
	
	public DBox (int abscissa, int ordinate, boolean accessibility,String label, Maze maze) {
		super(abscissa,ordinate,true,"D", maze);
	}
	
	public void setBackground(JPanel boxPanel, boolean escapeIsLaunched) {
		this.boxPanel = boxPanel;	
		boxPanel.setBackground(Color.RED);
	}

	public JPanel getBoxPanel() {
		return boxPanel;
	}
	
}
