package view.menu;

import javax.swing.JMenuBar;

import view.MazeApp;

public final class MazeMenuBar extends JMenuBar {

	// no need for a MazeApp reference for a menuBar
	private final Menu menu;
	
	public MazeMenuBar(MazeApp mazeApp) {
		super();
		add (menu = new Menu(mazeApp));
	}
}
