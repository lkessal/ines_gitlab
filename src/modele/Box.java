package modele;

public final class Box {
	
	private int ordinate;
	private int abscissa;
	private String boxLabel;
	
	public Box(int ordinate,int abscissa, String boxLabel) {
		this.boxLabel = boxLabel;
		this.abscissa = abscissa;
		this.ordinate = ordinate;
	}

	public int getOrdinate() {
		return ordinate;
	}

	public void setOrdinate(int ordinate) {
		this.ordinate = ordinate;
	}

	public int getAbscissa() {
		return abscissa;
	}

	public void setAbscissa(int abscissa) {
		this.abscissa = abscissa;
	}

	public String getBoxLabel() {
		return boxLabel;
	}

	public void setBoxLabel(String boxLabel) {
		this.boxLabel = boxLabel;
		
	}
	
}
