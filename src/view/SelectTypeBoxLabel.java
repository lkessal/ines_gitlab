package view;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public final class SelectTypeBoxLabel extends JLabel{
	
	private MazeApp mazeApp;
	
	public SelectTypeBoxLabel (MazeApp mazeApp) {
		//setLayout(new GridLayout(1,1));
		//JPanel jPanel = new JPanel();
		//JLabel string = new JLabel ("Select type of box");
		//jPanel.add(string);
		super ("Select the type of box");
		this.mazeApp = mazeApp;
		
	}

}
