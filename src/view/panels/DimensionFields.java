package view.panels;

import java.awt.GridLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import view.MazeApp;

public final class DimensionFields extends JPanel implements PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MazeApp mazeApp;
	private JFormattedTextField enterHeight;
	private JFormattedTextField enterWidth;
	private JLabel heightText;
	private JLabel widthText;
	private int height;
	private int width;
	
	
	
	public DimensionFields(MazeApp mazeApp) {
		
		setLayout(new GridLayout(1,2));
		this.mazeApp = mazeApp;
		this.heightText = new JLabel ("Enter height");
		this.widthText = new JLabel ("Enter width");
		this.enterHeight = new JFormattedTextField();
		this.enterWidth = new JFormattedTextField();
		
		//height = mazeApp.getModele().getHeight();
		//width =mazeApp.getModele().getWidth();
		enterHeight.setValue (10);
		enterWidth.setValue (10);
		
		enterHeight.addPropertyChangeListener("value", this);
		enterWidth.addPropertyChangeListener("value", this);
		
		JPanel heightPanel = new JPanel();
		heightPanel.setLayout(new GridLayout(1,2));
		heightPanel.add(heightText);
		heightPanel.add(enterHeight);
		
		JPanel widthPanel = new JPanel();
		widthPanel.setLayout(new GridLayout(1,2));
		widthPanel.add(widthText);
		widthPanel.add(enterWidth);
		
		add(widthPanel);
		add(heightPanel);
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		//getting the values entered by the user
		Integer heightInteger=(Integer)enterHeight.getValue();
		Integer widthInteger=(Integer)enterWidth.getValue();
		width = widthInteger.intValue();	
		height = heightInteger.intValue();
		//updating the model's attributes heightOfMaze and widthOfMaze
		mazeApp.getModele().setHeightOfMaze(height);
		mazeApp.getModele().setWidthOfMaze(width);
		System.out.println ("ds dimensionfields"+height);
		System.out.println ("ds dimensionfields"+width);
		
	}
	
}
