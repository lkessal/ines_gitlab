package dijkstra;

public interface ASetInterface {
	
	/* add a Vertex to A set*/
	public void add(VertexInterface vertex); 
	/*check if A contains vertex*/
	public boolean contains(VertexInterface vertex); 
	
}
