package modele;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import dijkstra.Dijkstra;
import dijkstra.Previous;
import dijkstra.PreviousInterface;
import dijkstra.VertexInterface;
import maze.DBox;
import maze.MBox;
import maze.Maze;
import maze.MazeReadingException;
import maze.WBox;

public final class Modele {

	private Maze maze;
	private MBox[][] mazeBoxes;
	private int widthOfMaze;
	private int heightOfMaze;
	private String selectedTypeBox = "E";
	private boolean modelModified;
	private boolean escapeIsLaunched;
	private boolean initMazeError;
	private String errorInitMessage;
	
	
	/*list of all the observers listening to the model's modifications*/
	private ArrayList<ChangeListener> listeners = new ArrayList<ChangeListener>() ;
	
	public Modele(int widthOfMaze, int heightOfMaze) {
			this.widthOfMaze = widthOfMaze;
			this.heightOfMaze = heightOfMaze;
			maze = new Maze(widthOfMaze,heightOfMaze, this);
			this.mazeBoxes = maze.getMaze();			
			//mazeBoxes[1][2]= new DBox(2, 2, true ,"D", maze);
	}
	
	

	public boolean getEscapeIsLaunched() {
		return escapeIsLaunched;
	}



	public void setEscapeIsLaunched(boolean escapeIsLaunched) {
		this.escapeIsLaunched = escapeIsLaunched;
	}



	public Maze getMaze() {
		return maze;
	}
	
	public int getWidthOfMaze() {
		return widthOfMaze;
	}

	public void setWidthOfMaze(int widthOfMaze) {
		if(this.widthOfMaze != widthOfMaze) {
			this.widthOfMaze = widthOfMaze;
			this.maze.setWidth(widthOfMaze);
			System.out.println("dans modele width="+this.maze.getWidth());
			//this.reset();
			modelModified = true;
			this.stateChanges();
		}
		else {};
	}

	public int getHeightOfMaze() {
		return heightOfMaze;
	}


	public void setHeightOfMaze(int heightOfMaze) {
		
		if(this.heightOfMaze != heightOfMaze) {
			this.heightOfMaze = heightOfMaze;
			this.maze.setHeight(heightOfMaze);
			System.out.println("dans modele height="+this.maze.getHeight());
			//this.reset();
			modelModified = true;
			this.stateChanges();
		}
		else {};
	}
	
	public void setMazeBox(MBox box) {
		MBox[][] mazeBoxes = maze.getMaze();
		mazeBoxes[box.getOrdinate()][box.getAbscissa()]=box;
		modelModified = true;
		stateChanges();
		
	}
	
	public void reset() {
		
		Box[][] newMazeBoxes = new Box[heightOfMaze][widthOfMaze]; 
		
		for (int i = 0; i<heightOfMaze; i++) {
			for (int j=0; j<widthOfMaze;j++) {
				newMazeBoxes[i][j]=new Box(i,j,"E");
			}
		}
		//this.setMazeBoxes(newMazeBoxes);
	}

	public String getSelectedTypeBox() {
		return selectedTypeBox;
	}

	public void setSelectedTypeBox(String selectedTypeBox) {
		this.selectedTypeBox = selectedTypeBox;
		System.out.println("selectedTypeBox=" +selectedTypeBox);
		modelModified = true;
	}

	public boolean getModelModified() {
		return modelModified;
	}

	public void setModelModified(boolean modelIsModified) {
		this.modelModified = modelIsModified;
	}
	
	public MBox[][] getMazeBoxes() {
		return maze.getMaze();
	}

	public void setMazeBoxes(MBox[][] mazeBoxes) {
		this.mazeBoxes = mazeBoxes;
		modelModified = true;
		this.stateChanges();
	}
	
	public void saveMaze() throws IOException{
		maze.saveToTextFile("data/savedMaze.txt");
	}
	
	public void escapeYourself() throws Exception, FileNotFoundException,IOException {
		this.saveMaze();
		//maze = new Maze(widthOfMaze,heightOfMaze);
		maze.initFromTextFile("data/savedMaze.txt");
		escapeIsLaunched = true;
		MBox departure = (MBox) maze.getDeparture();
		MBox arrival = (MBox) maze.getArrival();
		maze.saveToTextFile("data/labyrinthe2.txt");
		
		PreviousInterface previous = Dijkstra.dijkstra(maze,departure);
		Previous previousVertices = (Previous) previous;
		ArrayList<VertexInterface> verticesOfShortestPath = previousVertices.getShortestPath(arrival);
		maze.setVerticesOfShortestPath(verticesOfShortestPath);
		maze.saveShortestPath();
		this.stateChanges();
		//this.loadMaze("data/graphePlusCourtChemin.txt");
		escapeIsLaunched = false;
	}
	public void addObserver(ChangeListener listener) {
		listeners.add(listener) ;
	}
	
	/*method sending messages to observers when model is modified*/
	public void stateChanges() {
		ChangeEvent event = new ChangeEvent(this);
		for (ChangeListener listener : listeners) {
			listener.stateChanged(event);
		}
	}
	
	public void loadMaze(String fileName) throws Exception, IOException, FileNotFoundException {
		maze.initFromTextFile(fileName);
		//maze.saveToTextFile("data/labyrinthe2.txt");
		this.stateChanges();

	}
	
	public void initMazeError(String errorInitMessage) {
		initMazeError = true;
		this.errorInitMessage = errorInitMessage;
		this.stateChanges();
		initMazeError = false;
	}

	public String getErrorInitMessage() {
		return errorInitMessage;
	}

	public void setErrorInitMessage(String errorInitMessage) {
		this.errorInitMessage = errorInitMessage;
	}

	public boolean getInitMazeError() {
		return initMazeError;
	}

	public void setInitMazeError(boolean initMazeError) {
		this.initMazeError = initMazeError;
	}
	
//		
//		Box[][] newMazeBoxes = new Box[heightOfMaze][widthOfMaze];
//		try ( Reader reader = new FileReader (fileName);
//				BufferedReader buffer = new BufferedReader (reader);
//			){
//				String line = new String ();
//				
//				for (int i=0; i<heightOfMaze; i++) {
//					line = buffer.readLine();
//						
//						for (int j=0; j<widthOfMaze; j++) {
//							char box = line.charAt(j);
//							
//							/*initializing the boxes of MazeBoxes*/
//							switch(box) {
//								case 'A' : newMazeBoxes[i][j] = new Box(i,j,"A");
//										   break;
//								case 'D' : newMazeBoxes[i][j] = new Box(i,j,"D");
//										   break;
//								case 'E' : newMazeBoxes[i][j] = new Box(i,j,"E") ;
//										   break;
//								case 'W' : newMazeBoxes[i][j] = new Box(i,j,"W");
//									   break;
//								case 'V' : newMazeBoxes[i][j] = new Box(i,j,"V");
//										   break;
//								default : throw new Exception("Les lettres saisies sont incorrectes");
//						}
//						}
//						//lineCounter = lineCounter+1;
//				}
//				this.setMazeBoxes(newMazeBoxes);
//				buffer.close();
//				reader.close();
//				for (int i=0; i<heightOfMaze; i++) {
//					for (int j=0; j<widthOfMaze; j++) {
//						System.out.print(mazeBoxes[i][j].getBoxLabel());
//					}
//					System.out.print ("\n");
//				}
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//	}
}

