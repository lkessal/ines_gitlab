package view.panels;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.sun.xml.internal.ws.api.Component;

import dijkstra.VertexInterface;
import maze.MBox;
import maze.Maze;
import modele.Box;
import view.BoxMouseListener;
import view.MazeApp;

public final class MazePanel extends JPanel {

	private MazeApp mazeApp;
	private BoxPanel[][] mazePanelBoxes;
	private BoxMouseListener mazePanelMouseListener;
	private int width;
	private int height; 
	private GridLayout gridLayout;

	
	public MazePanel(MazeApp mazeApp) {
		this.mazeApp=mazeApp;		
		this.height = mazeApp.getModele().getHeightOfMaze();
		this.width = mazeApp.getModele().getWidthOfMaze();
		setLayout(gridLayout= new GridLayout(height,width,2,2));
		//initGraphicMaze(mazeApp.getModele().getMaze(),false, false);
		mazePanelMouseListener = new BoxMouseListener(mazeApp);
		addMouseListener(mazePanelMouseListener);
		
		mazePanelBoxes = new BoxPanel[height][width];
		MBox[][] modelBoxes = mazeApp.getModele().getMazeBoxes();
		for (int i=0; i<height; i++) {
			for (int j=0; j<width; j++) {
				mazePanelBoxes[i][j] = new BoxPanel(mazeApp,modelBoxes[i][j], j, i);
				mazePanelBoxes[i][j].setBackgroundInit();
				add(mazePanelBoxes[i][j]);
				System.out.println("MazeBoxes created");
			}
		}
		
	}
	
//	public void initGraphicMaze (Maze maze, boolean launchIsClicked, boolean initIsClicked) {
//		System.out.println("Hello6");		
//		for (int i=0; i<maze.getHeight(); i++) {
//			for (int j=0; j<maze.getHeight(); j++) {
//				/*getting the model's maze and then getting the box (i,j)*/
//				MBox box = mazeApp.getModele().getMaze().getBox(i,j);
//				/*getting the model's maze and then getting the label of box (i,j)*/
//				JButton button = new JButton();
//				box.setBackgroundColor(button,maze.getVerticesOfShortestPath(), launchIsClicked, initIsClicked) ;
//				add(button);
//				repaint();
//			}
//		}
//		this.setBorder(new EmptyBorder(50, 100, 100, 100));
//		this.mazeApp=mazeApp;
//    }
	
	public void notifyForUpdate() {
		if (this.width != mazeApp.getModele().getWidthOfMaze() || this.height != mazeApp.getModele().getHeightOfMaze()) {;
			this.width = mazeApp.getModele().getWidthOfMaze();
			this.height = mazeApp.getModele().getHeightOfMaze();
			gridLayout.setRows(height);
			gridLayout.setColumns(width);
			resetMaze();
			System.out.println ("reset du maze");
		}
		else {
			repaintMaze();
			System.out.println("Hello5");
		}
//		this.height = mazeApp.getModele().getHeightOfMaze();
//		this.width = mazeApp.getModele().getWidthOfMaze();		
//		if(mazeApp.getModele().getHeightOfMaze()!=height || mazeApp.getModele().getHeightOfMaze()!=width) {
//			resetMaze();
//			System.out.println("je suis la");
//		}
//		else {
//			System.out.println("je suis ici");
//		}
	}	
	
	public BoxPanel[][] getMazePanelBoxes() {
		return mazePanelBoxes;
	}

	public void setMazePanelBoxes(BoxPanel[][] mazePanelBoxes) {
		this.mazePanelBoxes = mazePanelBoxes;
	}

	public final void repaintMaze(){
		this.removeAll();
		System.out.print("height=" + height);
		System.out.println("width=" + width);
		//this.setMazePanelBoxes(new BoxPanel[height][width]);
		//gridLayout.setRows(height);
		//gridLayout.setColumns(width);
		
		MBox[][] modelBoxes = mazeApp.getModele().getMazeBoxes();
		for (int i=0; i<height; i++) {
			for (int j=0; j<width; j++) {
				mazePanelBoxes[i][j]= new BoxPanel(mazeApp,modelBoxes[i][j],j,i);
				if (mazePanelBoxes[i][j].getBox()==null) {
					mazePanelBoxes[i][j].setBackgroundInit();
					System.out.println("box null");
				}
				else {
					mazePanelBoxes[i][j].setBackground(mazeApp.getModele().getEscapeIsLaunched());
					System.out.println("abs="+modelBoxes[i][j].getAbscissa()+"et ordonnée="+modelBoxes[i][j].getOrdinate());
					System.out.println("pas null");
				}
				//mazePanelBoxes[i][j].notifyForUpdate(););
				add(mazePanelBoxes[i][j]);
			}
		}
		//need to put both because repaint() alone didn't refresh the view
		revalidate();
		repaint();		
	}
	
	public final void resetMaze(){
		this.removeAll();
		System.out.print("height=" + height);
		System.out.println("width=" + width);
		//this.setMazePanelBoxes(new BoxPanel[height][width]);
		//gridLayout.setRows(height);
		//gridLayout.setColumns(width);
		
		MBox[][] modelBoxes = mazeApp.getModele().getMazeBoxes();
		for (int i=0; i<height; i++) {
			for (int j=0; j<width; j++) {
				mazePanelBoxes[i][j]= new BoxPanel(mazeApp,modelBoxes[i][j],j,i);
				mazePanelBoxes[i][j].setBackgroundInit();
				//mazePanelBoxes[i][j].notifyForUpdate();
				add(mazePanelBoxes[i][j]);
			}
		}
		//need to put both because repaint() alone didn't refresh the view
		revalidate();
		repaint();		
	}
	
//	public void repaintMaze() {
//	Box[][] modelBoxes = mazeApp.getModele().getMazeBoxes();
//	for (int i=0; i<gridLayout.getRows(); i++) {
//		for (int j=0; j<gridLayout.getColumns(); j++) {
//			mazePanelBoxes[i][j] = new BoxPanel(mazeApp,modelBoxes[i][j]);
//			mazePanelBoxes[i][j].setBackground();
//			//mazePanelBoxes[i][j].notifyForUpdate();
//			add(mazePanelBoxes[i][j]);
//		}
//	}
//	revalidate();
//	repaint();
//	
//}

}
