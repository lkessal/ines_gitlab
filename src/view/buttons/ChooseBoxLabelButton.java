package view.buttons;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import view.MazeApp;

public final class ChooseBoxLabelButton extends JButton implements ActionListener{

	private MazeApp mazeApp;
	private String boxLabel;
	
	public ChooseBoxLabelButton(MazeApp mazeApp, String boxLabel) {
		super (boxLabel);
		this.mazeApp = mazeApp;
		addActionListener(this);
		this.boxLabel = boxLabel;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		mazeApp.getModele().setSelectedTypeBox(boxLabel);
		// TODO Auto-generated method stub
		
	}

	
}
