package view.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import view.MazeApp;

public class QuitMenuItem extends JMenuItem implements ActionListener {
	
	private MazeApp mazeApp;
	
	public QuitMenuItem(MazeApp mazeApp) {
		super("Quit");
		addActionListener(this);
		this.mazeApp = mazeApp;
		
	}

	@Override
	public void actionPerformed(ActionEvent event){
		mazeApp.dispose();			
	}

}
