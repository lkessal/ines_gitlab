package dijkstra;

import java.util.ArrayList;

public interface PreviousInterface {
	
	// getter of a vertex's father
	public VertexInterface getFather(VertexInterface vertex);
	// setter of a vertex's father // 
	public void setFather(VertexInterface vertexSon, VertexInterface vertexFather);
	// returns an ArrayList containing the vertices constituting a shortest path from the vertex Departure to vertexArrival
	public ArrayList <VertexInterface> getShortestPath (VertexInterface vertexArrival);
}
