package view;

import java.awt.Event;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import jdk.jfr.internal.tool.Main;
import maze.Maze;
import modele.Modele;
import view.menu.MazeMenuBar;
import view.panels.WindowPanel;

/*class implementing the app's frame*/

public  final class MazeApp extends JFrame implements ChangeListener{
	
		private final WindowPanel windowPanel;
		private Modele modele;
		private final MazeMenuBar mazeMenu;
		
		public MazeApp() {
			super ("Maze App");
			String heightString =  JOptionPane.showInputDialog(this, "Bienvenue dans Escape from the dungeon, saisissez la hauteur du labyrinthe");
			int height = Integer.parseInt(heightString);
			String widthString =  JOptionPane.showInputDialog(this, "Saisissez la largeur du labyrinthe");
			int width = Integer.parseInt(widthString);
			this.modele = new Modele(width, height);
			setContentPane(windowPanel = new WindowPanel(this));
			setJMenuBar(mazeMenu = new MazeMenuBar(this));
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			pack();
			setVisible(true);
			modele.addObserver(this);
		}
		

		public Modele getModele() {
			return modele;
		}

		public void setModele(Modele modele) {
			this.modele = modele;
		}
		
		public void stateChanged(ChangeEvent event) {
			
			if (modele.getInitMazeError() == true) {
				this.displayInitError(modele.getErrorInitMessage());
			}
			else {
				System.out.println("Hello3");
				windowPanel.notifyForUpdate();
			}
		}
		
		public void displayInitError (String errorMessage) {
			JOptionPane errorDisplay = new JOptionPane();
			errorDisplay.showMessageDialog(this, errorMessage + ". Merci de corriger cette erreur");
		}
		
}
