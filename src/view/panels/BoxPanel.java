package view.panels;

import java.awt.Color;

import javax.swing.JPanel;

import maze.MBox;
import modele.Box;
import view.BoxMouseListener;
import view.MazeApp;


public class BoxPanel extends JPanel {
	
	private MazeApp mazeApp;
	private BoxMouseListener boxMouseListener;
	private MBox box;
	private int abscissa;
	private int ordinate;
	
	public BoxPanel(MazeApp mazeApp, MBox box, int abscissa, int ordinate) {
		this.mazeApp = mazeApp;
		boxMouseListener = new BoxMouseListener(mazeApp);
		addMouseListener(boxMouseListener);
		this.box = box;
		this.abscissa = abscissa;
		this.ordinate =ordinate;
	}
	
	public int getAbscissa() {
		return abscissa;
	}


	public void setAbscissa(int abscissa) {
		this.abscissa = abscissa;
	}


	public int getOrdinate() {
		return ordinate;
	}


	public void setOrdinate(int ordinate) {
		this.ordinate = ordinate;
	}


	public void setBackgroundInit() {
		this.setBackground(Color.CYAN);
	}
	
	public MBox getBox() {
		return box;
	}
	
	public void setBox(MBox box) {
		this.box = box;
	}
	
	public void setBackground(boolean escapeIsLaunched) {
		box.setBackground(this, escapeIsLaunched);
//		if (box.getLabel().equals("A")) {
//			this.setBackground(Color.ORANGE);
//			System.out.println("je suis ds background arrival");
//			System.out.println("abs="+box.getAbscissa()+"ordonée="+box.getOrdinate());
//			repaint();
//		}
//		if (box.getLabel().equals("D")) {
//			this.setBackground(Color.RED);
//			System.out.println("je suis ds background red");
//			System.out.println("abs="+box.getAbscissa()+"ordonée="+box.getOrdinate());
//			repaint();
//		}
//		if (box.getLabel().equals("W")) {
//			this.setBackground(Color.BLACK);
//			System.out.println("je suis ds background wall");
//			System.out.println("abs="+box.getAbscissa()+"ordonée="+box.getOrdinate());
//			repaint();
//		}
//		if (box.getLabel().equals("E")) {
//			this.setBackground(Color.GREEN);
//			System.out.println("je suis ds background empty");
//			System.out.println("abs="+box.getAbscissa()+"ordonée="+box.getOrdinate());
//			repaint();
//		}
//		if (box.getLabel().equals("V")) {
//			this.setBackground(Color.BLUE);
//			System.out.println("je suis ds background solved");
//			System.out.println("abs="+box.getAbscissa()+"ordonée="+box.getOrdinate());
//			repaint();
//		}
	}
	
}
