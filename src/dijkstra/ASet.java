package dijkstra;

import java.util.HashSet;

/*ASetClass represents the set containing the vertices of a shortest path from Departure to Arrival*/
public final class ASet implements ASetInterface{
	
	private HashSet<VertexInterface> aSet;
	
	public ASet() {
		aSet = new HashSet<VertexInterface>();
	}
	
	/* add a Vertex to A set*/
	public void add(VertexInterface vertex) {
		
		aSet.add(vertex);
	}
	/*check if A contains vertex*/
	public boolean contains(VertexInterface vertex) {
		
		if (aSet.contains(vertex)){ return true;}
		else {return false;}
	}
	

}
