package dijkstra;

import java.util.ArrayList;
import java.util.Hashtable;

public final class Previous implements PreviousInterface {

	private Hashtable <VertexInterface, VertexInterface> previousTable;
	
	public Previous () {
		previousTable = new Hashtable <VertexInterface, VertexInterface> ();
	}
	
	public VertexInterface getFather(VertexInterface vertex) {
		return previousTable.get(vertex);
	}

	public void setFather(VertexInterface vertexSon, VertexInterface vertexFather) {
		previousTable.put(vertexSon,vertexFather);
	}
	public ArrayList <VertexInterface> getShortestPath (VertexInterface vertexArrival){
		
		ArrayList<VertexInterface> verticesOfShortestPath = new ArrayList<VertexInterface>();
		verticesOfShortestPath.add(vertexArrival);
		VertexInterface vertex = vertexArrival;
		
		while (vertex != null) {
			verticesOfShortestPath.add(vertex);
			vertex = previousTable.get(vertex);
		}
		
		return verticesOfShortestPath;
	}
	
	public Hashtable <VertexInterface, VertexInterface> getTable(){
				return previousTable;
	}
}
