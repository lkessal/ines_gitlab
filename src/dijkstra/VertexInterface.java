package dijkstra;

public interface VertexInterface {
	
	// returns the label of a vertex
	public String getLabel ();
}
