package view.buttons;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import view.MazeApp;

public final class InitButton extends JButton implements ActionListener{

		private MazeApp mazeApp;
		private boolean initIsClicked=false;
		
		public InitButton (MazeApp mazeApp) {
			super ("Reset the maze");
			this.mazeApp=mazeApp;
			addActionListener(this);
		}
		
		public void actionPerformed (ActionEvent event) {
			initIsClicked = true;
			mazeApp.getModele().stateChanges();
			initIsClicked = false;
		}

		public boolean getInitIsClicked() {
			return initIsClicked;
		}
}
