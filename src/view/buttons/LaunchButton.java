package view.buttons;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import view.MazeApp;

public final class LaunchButton extends JButton implements ActionListener{

		private MazeApp mazeApp;
		private boolean launchIsClicked = false;
		
		public LaunchButton (MazeApp mazeApp) {
			super ("Escape yourself");
			this.mazeApp=mazeApp;
			addActionListener(this);
		}
		
		public void actionPerformed (ActionEvent event) {
			//launchIsClicked = true;
			try {
				mazeApp.getModele().escapeYourself();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Hello1");
		}
		
		public boolean getLaunchIsClicked() {
			return launchIsClicked;
		}
}
