package view.panels;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import view.MazeApp;

public final class ExplanationPanel extends JPanel {

	private final MazeApp mazeApp;
	private JLabel intro;
	private JLabel firstStep;
	private JLabel secondStep;
	private JLabel thirdStep;
	private JLabel fourthStep;
	private JLabel fifthStep;
	private JLabel sixthStep;
	private JLabel seventhStep;
	
	public ExplanationPanel(MazeApp mazeApp) {
		
		super();
		this.mazeApp = mazeApp;
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		this.add(intro = new JLabel ("How to play"));
		this.add(Box.createVerticalGlue());
		this.add(firstStep = new JLabel ("1. First option : Design your own maze : "));
		this.add(Box.createVerticalGlue());
		this.add(secondStep = new JLabel ("For each box, select the type of box you want to set with the buttons below and press the corresponding box"));
		this.add(Box.createVerticalGlue());
		this.add((thirdStep = new JLabel ("(you can put one departure, one arrival, and as many walls and empty boxes you want")));
		this.add(Box.createVerticalGlue());
		this.add(fourthStep =new JLabel ("2. Second option : Initialize your maze into the data/loadNewMaze.txt file and load it from the menu"));
		this.add(Box.createVerticalGlue());
		this.add(fifthStep = new JLabel ("3.Press the button Escape Yourself : the shortest path to escape from the dungeon will appear in blue"));
		this.add(Box.createVerticalGlue());
		this.add(sixthStep = new JLabel ("If nothing appear, it means the maze has no solution : you can't escape"));
		this.add(Box.createVerticalGlue());
		this.add(seventhStep = new JLabel ("4. You can save the maze with the save maze menu item, and the maze's solution with the save maze's solution menu item"));
		this.add(Box.createVerticalGlue());
	}
}