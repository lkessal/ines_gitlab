package dijkstra;

import java.util.ArrayList;

public interface GraphInterface {
	
	/*returns the successors of a Vertex*/
	public ArrayList<VertexInterface> getSuccessors(VertexInterface vertex) ;
	/*returns the weight of the edge linking two vertices*/
	public int getWeight(VertexInterface src,VertexInterface dst);
	/*returns all the vertices*/
	public ArrayList<VertexInterface> getAllVertices();
}
