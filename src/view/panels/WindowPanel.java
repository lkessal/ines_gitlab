package view.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import view.MazeApp;
import view.buttons.InitButton;
import view.buttons.LaunchButton;

/*class implementing a panel into the frame*/

public final class WindowPanel extends JPanel {
	
	private ButtonsPanel buttonsPanel;
	private MazePanel mazePanel;
	private TitlePanel titlePanel;;
	private LaunchButton launchButton;
	private ExplanationPanel explanationPanel;
	private LaunchAndExplainationPanel launchAndExplainationPanel;
	
	public WindowPanel (MazeApp mazeApp) {
		setLayout(new BorderLayout());
		setBackground (Color.WHITE);
		buttonsPanel = new ButtonsPanel(mazeApp);
		add (buttonsPanel = new ButtonsPanel(mazeApp), BorderLayout.SOUTH);
		add (titlePanel = new TitlePanel(mazeApp), BorderLayout.NORTH);
		add (mazePanel = new MazePanel(mazeApp), BorderLayout.CENTER);
		// adding dimensionPanel into an intermediate panel to limit dimensionContainer's size
		//JPanel launchInitContainer = new JPanel();
		//launchInitContainer.setLayout(new BoxLayout(launchInitContainer, BoxLayout.Y_AXIS));
		//launchInitContainer.add(explanationPanel = new ExplanationPanel(mazeApp));
		//launchInitContainer.add(launchButton=new LaunchButton(mazeApp));
		//launchInitContainer.add(initButton=new InitButton(mazeApp));
		//JPanel explanationContainer = new JPanel();
		//add (explanationContainer, BorderLayout.EAST);
		add (launchAndExplainationPanel = new LaunchAndExplainationPanel(mazeApp), BorderLayout.EAST);

	
	}
	
	public MazePanel getMazePanel() {
		return mazePanel;
	}

	public void setMazePanel(MazePanel mazePanel) {
		this.mazePanel = mazePanel;
	}

	public TitlePanel getTitlePanel() {
		return titlePanel;
	}

	public void setTitlePanel(TitlePanel titlePanel) {
		this.titlePanel = titlePanel;
	}

	public ButtonsPanel getButtonsPanel() {
		return buttonsPanel;
	}

	public void setButtonsPanel(ButtonsPanel buttonsPanel) {
		this.buttonsPanel = buttonsPanel;
	}
	
	
	public void notifyForUpdate() {
		System.out.println("Hello4");
		mazePanel.notifyForUpdate();
		//buttonsPanel.notifyForUpdate();
	}

}
