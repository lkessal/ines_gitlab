package view.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import view.MazeApp;

public class SaveSolutionMenuItem extends JMenuItem implements ActionListener {
	
	private MazeApp mazeApp;
	private JOptionPane popupMessage;
	
	public SaveSolutionMenuItem(MazeApp mazeApp) {
		super("Save the maze's solution");
		addActionListener(this);
		this.mazeApp = mazeApp;
		popupMessage = new JOptionPane();
		
	}

	@Override
	public void actionPerformed(ActionEvent event){
		try {
			System.out.println("maze saved");
			mazeApp.getModele().getMaze().saveShortestPath();;
			popupMessage.showMessageDialog(mazeApp, "Maze's solution saved in data/graphePlusCourtChemin.txt");
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}
}
