package view.menu;

import javax.swing.JMenu;

import view.MazeApp;

public final class Menu extends JMenu{
	
		// no need for a MazeApp reference for a menu
		private final SaveMenuItem saveMenuItem;
		private final LoadMenuItem loadMenuItem;
		private LoadLastMazeMenuItem loadLastMazeMenuItem;
		private SaveSolutionMenuItem saveSolutionMenuItem;
		private QuitMenuItem quitMenuItem;

		public Menu (MazeApp mazeApp){
			super("Save/load a maze");
			add(saveMenuItem = new SaveMenuItem(mazeApp));
			add(loadMenuItem = new LoadMenuItem(mazeApp));
			add (loadLastMazeMenuItem = new LoadLastMazeMenuItem(mazeApp));
			add (saveSolutionMenuItem = new SaveSolutionMenuItem(mazeApp));
			add (quitMenuItem = new QuitMenuItem(mazeApp));
		}
}
