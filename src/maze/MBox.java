package maze;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import dijkstra.VertexInterface;

/*MBox abstract class represents the properties of the maze's boxes (declared Abstract because class not used to instantiate objects*/

public abstract class MBox 
	implements VertexInterface{
	/* attributes declared final to optimize compilation*/
	private int abscissa;
	private int ordinate;
	private final boolean accessibility;
	private final String label;
	protected final Maze maze;
	private JPanel boxPanel;

	
	/*constructor to initialize the MBox object's attributes*/
	
	public MBox (int abscissa, int ordinate, boolean accessibility, String label, Maze maze) {
		this.abscissa = abscissa;
		this.ordinate = ordinate;
		this.accessibility=accessibility;
		this.label=label;
		this.maze=maze;
	}
	
	public MBox(boolean accessibility, String label, Maze maze) {
		this.accessibility=accessibility;
		this.label=label;
		this.maze=maze;
	}
	
	/*getters to get the label, abscissa and ordinate of an MBox Object (some methods declared final to optimize compilation)*/
	
	public final String getLabel() {
		return label;
	}
	
	public final int getAbscissa() {
		return abscissa;
	}
	
	public final int getOrdinate(){
		return ordinate;
	}
	
	public boolean getAccessibility() {
		return accessibility;
	};
	
	public final Maze getMaze() {
		return maze;
	}
	
	public abstract void setBackground (JPanel boxPanel, boolean escapeIsLaunched);
	
	public abstract JPanel getBoxPanel();
}

