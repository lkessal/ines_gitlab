package dijkstra;

import java.util.Hashtable;
/* Pi class represents the function calculating the shortest valuation to reach a vertex from the departure*/
public final class Pi implements PiInterface {
	
	private Hashtable<VertexInterface, Integer> piTable;
	
	public Pi() {
		piTable = new Hashtable<VertexInterface, Integer>();
	}
	
	//setter of pi(vertex) = weight of a shortest path from departure to vertex 
	public void setPi(VertexInterface vertex, int shortValuation) {
		Integer valuation = new Integer(shortValuation);
		piTable.put(vertex, valuation);
	}
	//getter of pi(vertex)
	public int getPi (VertexInterface vertex) {
		return piTable.get(vertex);
	}
}
