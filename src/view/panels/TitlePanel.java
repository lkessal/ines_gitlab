package view.panels;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import view.MazeApp;

public final class TitlePanel extends JLabel{

	private MazeApp mazeApp;
	
	public TitlePanel(MazeApp mazeApp) {
		super("Escape from the dungeon !");
		this.mazeApp = mazeApp;
		setSize(new Dimension(100,100));
		this.setBorder(new EmptyBorder(20,0,0,0));
	}
	
}
