package maze;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import dijkstra.VertexInterface;

/*EBox class represents the properties of the maze's  empty boxes*/

public final class EBox extends MBox{

	private JPanel boxPanel;
	
	public EBox (int abscissa, int ordinate, boolean accessibility,String label,Maze maze) {
		super(abscissa,ordinate,true,"E", maze);
	}
	
	public void setBackground (JPanel boxPanel, boolean escapeIsLaunched) {
		boxPanel.setBackground(Color.green);
//		this.button = button;
		if(maze.getVerticesOfShortestPath()!=null) {
			if (maze.getVerticesOfShortestPath().contains(this)== true && escapeIsLaunched){
			 System.out.println("Hello8");
			 boxPanel.setBackground(Color.blue);
			}
			else{
				 System.out.println("Hello10");
				 boxPanel.setBackground(Color.green);
			}
		}
		else{
			 System.out.println("Hello9");
			 boxPanel.setBackground(Color.green);
		 }
	}

	public JPanel getBoxPanel() {
		return boxPanel;
	}

	public void setBoxPanel(JPanel boxPanel) {
		this.boxPanel = boxPanel;
	}
}
