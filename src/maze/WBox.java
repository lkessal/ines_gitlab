package maze;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import dijkstra.VertexInterface;

/*WBox class represents the properties of the maze's  wall boxes*/

public final class WBox extends MBox{

	private JPanel boxPanel;
	
	public WBox (int abscissa, int ordinate, boolean accessibility,String label,Maze maze) {
		super(abscissa,ordinate,false,"W",maze);
	}
	
	public void setBackground(JPanel boxPanel, boolean escapeIsLaunched) {
		boxPanel.setBackground(Color.BLACK);
			 this.boxPanel = boxPanel;
	}
	
	public JPanel getBoxPanel() {
		return boxPanel;
	}
}