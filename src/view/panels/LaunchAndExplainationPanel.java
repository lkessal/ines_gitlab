package view.panels;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import view.MazeApp;
import view.buttons.LaunchButton;

public final class LaunchAndExplainationPanel extends JPanel {
	
	private final MazeApp mazeApp;

	private ExplanationPanel explanationPanel;
	private LaunchButton launchButton;
	
	public LaunchAndExplainationPanel(MazeApp mazeApp){
		super();
		this.mazeApp = mazeApp;
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(explanationPanel = new ExplanationPanel(mazeApp));
		this.add(launchButton=new LaunchButton(mazeApp));
		}
}
