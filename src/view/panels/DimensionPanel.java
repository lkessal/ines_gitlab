package view.panels;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import view.MazeApp;

public final class DimensionPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DimensionFields dimensionFields;
	private MazeApp mazeApp;
	
	public DimensionPanel(MazeApp mazeApp){
		setLayout(new BorderLayout());
		add(dimensionFields = new DimensionFields(mazeApp));
		this.mazeApp = mazeApp;
		
	}
	
	public void notifyForUpdate() {
	//	dimensionFields.notifyForUpdate();
	}
}
