package maze;

public final class MazeReadingException extends Exception {

	public MazeReadingException (String fileName, int lineError, String errorMessage) {
		super ("Une erreur s'est produite lors de l'initialisation du labyrinthe dans le fichier texte " + fileName+ " à la ligne" + lineError+ " : " + errorMessage);
	}
}
